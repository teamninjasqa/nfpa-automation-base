describe('validate NFPA Application',()=>{

    beforeEach(() => {
       cy.setCookie('authtoken', 'eyJrZXlpZCI6Imh3cHJvcHUiLCJhbGciOiJIUzUxMiJ9.eyJsYXN0TmFtZSI6IkFydGhpbWFsbGEiLCJzdWIiOiJWVU44TUROOGMybDJZWEpoYlVCMVkyUmhkbWx6TG1Wa2RRIiwidXNhZ2UiOiJVU0VSIiwiaXNzIjoiaHR0cDovL2Rldi5yaXNrYW5kc2FmZXR5LmNvbS9lcm1zcCIsImF1ZCI6IlVDUlNTIiwiZmlyc3ROYW1lIjoiU2l2YXJhbSIsImV4cCI6MTU2MTYyOTk2MSwiaWF0IjoxNTYxNTQzNTYxLCJ0ZW5hbnQiOiJVQyIsImVudGl0eSI6IjAzIiwiZW1haWwiOiJzYXJ0aGltYWxsYUB1Y2RhdmlzLmVkdSIsImp0aSI6IjgyMjQ4NDQ4LWM4ZjgtNDAzOC1hZjkxLWVlZGJjZGI4YzNkZCIsImNpZCI6InNpdmFyYW1AdWNkYXZpcy5lZHUifQ.Sl9riKwXM1YTv5xJRKH8GVyDykvzUPQwcMLDc6kII_t56tj-txFZ11lJ6s5Sp7C-att0HShYefQrMemQH85khg')
    });

    it('login as nfpa admin',()=>{
        cy.visit('https://qa.riskandsafety.com/nfpa');
        cy.get(".jss105").contains('NFPA MENU');
        cy.get("header").contains("Surveys");
    });

    it("Click on Create new survey button",()=>{
        cy.get("#btnNewSurvey").click();
        cy.get("header").contains("NFPA");
        cy.get("p").first().should('contain','UC Annual Emergency Management Survey FY 2018-19');
    });

    it("Select options for Program Management",()=>{
        cy.get(".jss197 > :nth-child(4)").should('contain','Program Management');
        cy.get(':nth-child(5) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(6) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(7) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(8) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(9) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(10) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Program Coordinator/Manager",()=>{
        cy.get('.jss197 > :nth-child(12)').should('contain','Program Coordinator/Manager');
        cy.get(':nth-child(13) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();           
    });

    it("Select options for Compliance with Laws/Requirements",()=>{
        cy.get('.jss197 > :nth-child(15)').should('contain','Compliance with Laws/Requirements');
        cy.get(':nth-child(16) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();   
        cy.get(':nth-child(17) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click(); 
    });

    it("Select options for Finance and Administration",()=>{
        cy.get('.jss197 > :nth-child(19)').contains('Finance and Administration');
        cy.get(':nth-child(20) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click(); 
        cy.get(':nth-child(21) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(22) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(23) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Planning and Design Process",()=>{
        cy.get('.jss197 > :nth-child(25)').contains("Planning and Design Process");
        cy.get(':nth-child(26) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(27) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(28) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(29) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(30) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Hazard Vulnerability Assessment",()=>{
        cy.get('.jss197 > :nth-child(32)').contains("Hazard Vulnerability Assessment");
        cy.get(':nth-child(33) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(34) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(35) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(36) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(37) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(38) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Business Impact Analysis (BIA)",()=>{
        cy.get('.jss197 > :nth-child(40)').contains("Business Impact Analysis (BIA)");
        cy.get(':nth-child(41) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(42) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(43) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(44) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Resource Needs Assessment",()=>{
        cy.get('.jss197 > :nth-child(46)').contains("Resource Needs Assessment");
        cy.get(':nth-child(47) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(48) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(49) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(50) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(51) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Incident Prevention and Hazard Mitigation",()=>{
        cy.get('.jss197 > :nth-child(53)').contains("Incident Prevention and Hazard Mitigation");
        cy.get(':nth-child(54) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(55) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(56) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(57) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });
    
    it("Select options for Crisis Communications and Public Information",()=>{
        cy.get('.jss197 > :nth-child(59)').contains("Crisis Communications and Public Information");
        cy.get(':nth-child(60) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(61) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Warning Notifications and Communications",()=>{
        cy.get('.jss197 > :nth-child(63)').contains("Warning Notifications and Communications");
        cy.get(':nth-child(64) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(65) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(66) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Standard Operating Procedures (SOPs)",()=>{
        cy.get('.jss197 > :nth-child(68)').contains("Standard Operating Procedures (SOPs)");
        cy.get(':nth-child(69) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(70) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(71) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(72) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Incident Management",()=>{
        cy.get('.jss197 > :nth-child(74)').contains("Incident Management");
        cy.get(':nth-child(75) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(76) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(77) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(78) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(79) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(80) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(81) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(82) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Emergency Operations/Response Plan",()=>{
        cy.get('.jss197 > :nth-child(84)').contains("Emergency Operations/Response Plan");
        cy.get(':nth-child(85) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(86) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(87) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Continuity and Recovery",()=>{
        cy.get('.jss197 > :nth-child(89)').contains("Continuity and Recovery");
        cy.get(':nth-child(90) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(91) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(92) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(93) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Employee Assistance and Support",()=>{
        cy.get('.jss197 > :nth-child(95)').contains("Employee Assistance and Support");
        cy.get(':nth-child(96) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(97) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Training and Education",()=>{
        cy.get('.jss197 > :nth-child(99)').contains("Training and Education");
        cy.get(':nth-child(100) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(101) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(102) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(103) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Exercises and Tests",()=>{
        cy.get('.jss197 > :nth-child(105)').contains("Exercises and Tests");
        cy.get(':nth-child(106) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(107) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Select options for Program Maintenance and Improvement",()=>{
        cy.get('.jss197 > :nth-child(109)').contains("Program Maintenance and Improvement");
        cy.get(':nth-child(110) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(111) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(112) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
        cy.get(':nth-child(113) > .jss434 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="Non-Conforming"]').last().click();
    });

    it("Fill all you general information",()=>{
        cy.get('.jss533').contains("General Information");
        cy.get('#title').type("Test");
        cy.get('#phone').type("9876543210");
        cy.get('#firstName').clear().type("Sivaram");
        cy.get('#email').clear().type("sarthimalla@ucdavis.edu");
        cy.get('#lastName').clear().type("Arthimalla");
        cy.get('.jss534 > .jss505 > .jss485 > .jss486').click();
        cy.get('[data-value="UC Davis"]').click();
        cy.get('#accomplishment').type("Comments")
    })

    // it("Submit a form",()=>{
    //     cy.get('#btnSubmit').click();
    // })
})